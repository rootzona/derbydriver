package dlm_commands;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) throws Exception {
        //Registering the driver
        Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        //Getting the Connection object
        String URL = "jdbc:derby:sampledatabase;create=true";
        Connection conn = DriverManager.getConnection(URL);
        //Creating the Statement object
        Statement stmt = conn.createStatement();
        //Query to create a table in JavaDB database
        String query = "CREATE TABLE Employee( "
                + "Id INT NOT NULL GENERATED ALWAYS AS IDENTITY, "
                + "Name VARCHAR(255), "
                + "Salary INT NOT NULL, "
                + "Location VARCHAR(255), "
                + "PRIMARY KEY (Id))";
        //Executing the query
        stmt.execute(query);
        System.out.println("Table created");
    }
}
